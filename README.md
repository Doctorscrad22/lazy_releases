# lazy_releases
This repository contains all builds of Lazy kernel for custom ROMS and OOS.

# Forum and Group links
- XDA Thread - https://forum.xda-developers.com/t/lazy-kernel-for-op5-t-10-27-2020.4077467/
- Telegram Support Group - https://t.me/lazykernel
- Telegram Kernel Release Group - https://t.me/lazyrelease

# Version Guide
- Stable release:
  - lazy-custom-v2.4-20210815-1
- Test release (only posted in telegram):
  - lazy-custom-v2.4-20210815-1-test

# Branches
All versions are based on weeb kernel.

- custom - Newer LEDS QPNP haptic drivers
  - Supports: Android 11 custom ROMs with the new haptic drivers (contact maintainer or developer)
- custom-old - Old QPNP haptic drivers
  - Supports: Android 9-11 custom ROMs without support for the new haptic drivers
- oos - Self-explanatory
  - Supports: OOS
- pa - OOS + PA touchscreen and USB driver adjustments
  - Supports: PA Quartz 3+

# Licence
Copyright (c), The Linux Foundation. All rights reserved.

 This software is licensed under the terms of the GNU General Public
 License version 2, as published by the Free Software Foundation, and
 may be copied, distributed, and modified under those terms.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
